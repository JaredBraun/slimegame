﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using ParadoxNotion.Design;

[Name("Sleep")]
[Category("Slime Actions")]
[Description("Make the slime sleep at a certain time.")]
public class Sleep : ActionTask<AIController> {

    public float timeRange; 

    protected override void OnExecute() { }
    protected override void OnUpdate() { }


}
