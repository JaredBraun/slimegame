﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using NodeCanvas.Framework;
using ParadoxNotion.Design;

[Name("Wander")]
[Category("Slime Actions")]
[Description("Make the slime wander around in a radius that is assigned to AI Controller.")]
public class Wander : ActionTask<AIController> {

    private Vector3 wanderNextPosition;
    private NavMeshAgent navMesh;

    protected override void OnExecute() {
        navMesh = agent.navMesh;
        ChooseWanderPos(); }
    protected override void OnUpdate() { WanderBehavior(); }

    void WanderBehavior()
    {
        if (navMesh.destination != wanderNextPosition)
            navMesh.SetDestination(wanderNextPosition);

        if (!HasReachedDestination()) return;
        EndAction(true);
        ChooseWanderPos();
    }

    void ChooseWanderPos()
    {
        Vector3 currPos = agent.transform.position;
        wanderNextPosition = currPos + GetRandomWanderDirection();
    }

    private Vector3 GetRandomWanderDirection()
    {
        Vector3 randAngle = Random.onUnitSphere;
        randAngle.y = 0;
        float randDistance = Random.Range(agent.wanderRadius.x, agent.wanderRadius.y);
        return randAngle * randDistance;
    }

    private bool HasReachedDestination()
    {
        return navMesh.remainingDistance <= navMesh.stoppingDistance;
    }

    public override void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(wanderNextPosition, 0.5f);
    }

}
