﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

public enum SlimeActions { Idling, Wandering, Eating, Pulling }

public class AIController : MonoBehaviour {

    public Transform meshAnchor;
    public MeshRenderer mesh;

    [Header("Settings")]
    public Vector2 idleTimeRange;
    public Vector2 wanderRadius;
    public List<FoodTypes> edibleFood;
    public Material[] emotionMaterials;

    public int maxEatAmount = 3;
    [Range(0, 1)]
    public float rateOfGrowth;
    public float eatingDistance = 0.8f;
    
    public bool runAnimation = true;

    public AudioClip[] landingSounds;
    public AudioClip[] jumpingSounds;
    public AudioClip[] eatingSounds;

    public LayerMask groundLayer;
    public float distanceToGround = 0.2f;

    [Header("Stats")]
    public SlimeActions currAction;
    public SlimeActions oldAction;
    public bool isGrounded;
    public bool isPulled;
    public bool isNearFood;
    public GameObject currentFood;
    public int currEatAmount;

    //Hidden Variables
    [HideInInspector]
    public NavMeshAgent navMesh;
    [HideInInspector]
    public Animator anim;

    private void Start()
    {
        navMesh = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        GetComponent<Collectable>().OnPulled.AddListener(Pulling);
        oldAction = currAction;
    }

    private void Update()
    {
        if (runAnimation) CheckAnimation();

        //Checking to changing emotion
        if (oldAction != currAction) {
            CheckEmotion();
            oldAction = currAction;
        }

        CheckGround();
    }

    private void CheckAnimation()
    {
        anim.SetBool("isIdle", currAction == SlimeActions.Idling);
        anim.SetBool("isWander", currAction == SlimeActions.Wandering);
        anim.SetBool("isPulling", currAction == SlimeActions.Pulling);
        anim.SetBool("isEating", currAction == SlimeActions.Eating);
    }

    #region Triggers

    private void CheckGround()
    {
        if (Physics.Raycast(transform.position + 
            Vector3.down * transform.localScale.y / 3, 
            Vector3.down, distanceToGround, groundLayer))
        {
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }
        Debug.DrawRay(transform.position +
            Vector3.down * transform.localScale.y / 3, Vector3.down * distanceToGround, Color.yellow);
    }

    private void OnTriggerStay(Collider coll)
    {
        if (coll.GetComponent<Food>() != null && currEatAmount < maxEatAmount)
        {
            SetCurrentFood(coll.gameObject);
        }
    }

    private void OnTriggerExit(Collider coll)
    {
        if (coll.GetComponent<Food>() != null && isNearFood)
        {
            if (currentFood == coll.gameObject)
            {
                isNearFood = false;
                currentFood = null;
            }
        }
    }
    #endregion

    #region Food Functions
    public void DestroyFood()
    {
        if (currentFood != null)
        {
            currentFood.GetComponent<Food>().MoveToPlayer(transform);
        }
    }

    public void SetCurrentFood(GameObject newFood)
    {
        Food foodScript = newFood.GetComponent<Food>();

        if (edibleFood.Contains(foodScript.type) && !isNearFood && !foodScript.isTaken)
        {
            isNearFood = true;
            currentFood = newFood.gameObject;
        }
    }

    public void Grow()
    {
        meshAnchor.DOScale(meshAnchor.transform.localScale.x + rateOfGrowth, 0.5f).SetEase(Ease.OutElastic);
    }
    #endregion

    #region Emotion Functions
    //Changing emotion based on current actions
    private void CheckEmotion()
    {
        //Hard numbers
        if (currAction == SlimeActions.Idling) SetEmotion(2);
        if (currAction == SlimeActions.Eating) SetEmotion(0);
        if (currAction == SlimeActions.Wandering) SetEmotion(4);
        if (currAction == SlimeActions.Pulling) SetEmotion(1);
    }

    public void SetEmotion(int emojiIndex)
    {
        if (emojiIndex < emotionMaterials.Length)
            mesh.sharedMaterial = emotionMaterials[emojiIndex];
    }
    #endregion

    #region Animation Functions
    public void LandOnGround()
    {
        PlayRandomSound(landingSounds);
        SetEmotion(1);
    }

    public void JumpFromGround()
    {
        PlayRandomSound(jumpingSounds);
        SetEmotion(0);
    }

    public void Eating()
    {
        PlayRandomSound(eatingSounds);
    }
    #endregion

    void PlayRandomSound(AudioClip[] audioClips)
    {
        int index = Random.Range(0, audioClips.Length);
        GetComponent<AudioSource>().PlayOneShot(audioClips[index]);
    }

    public void Pulling()
    {
        isPulled = !isPulled;
    }

    /// <summary>
    /// Debugging
    /// </summary>
    public void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(transform.position, wanderRadius.x);
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(transform.position, wanderRadius.y);
    }
}
