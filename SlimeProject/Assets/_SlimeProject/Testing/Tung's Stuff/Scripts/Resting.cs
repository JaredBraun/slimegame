﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using ParadoxNotion.Design;

[Name("Rest")]
[Category("Slime Actions")]
[Description("Make the slime to rest.")]
public class Resting : ActionTask <AIController> {

    private float secondsToRun;

    protected override void OnExecute()
    {
        secondsToRun = Random.Range(agent.idleTimeRange.x, agent.idleTimeRange.y);
    }

    protected override void OnUpdate()
    {
        secondsToRun = Random.Range(agent.idleTimeRange.x, agent.idleTimeRange.y);
        if (elapsedTime > secondsToRun) EndAction(true);
    }
}
