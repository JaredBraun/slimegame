﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using NodeCanvas.Framework;
using ParadoxNotion.Design;

[Name("Eat")]
[Category("Slime Actions")]
[Description("Eat the current food.")]
public class Eat : ActionTask<AIController>
{

    public BBParameter<float> eatingTime = 1;

    protected override void OnExecute()
    {

    }

    protected override void OnUpdate()
    {
        if (agent.navMesh.destination != agent.currentFood.transform.position)
            MoveTowardFood();

        if (!HasReachedFood()) return;


    }

    void MoveTowardFood()
    {
        agent.navMesh.SetDestination(agent.currentFood.transform.position);
    }

    void EatFood()
    {

    }

    private bool HasReachedFood()
    {
        return agent.navMesh.remainingDistance <= agent.navMesh.stoppingDistance;
    }
}
