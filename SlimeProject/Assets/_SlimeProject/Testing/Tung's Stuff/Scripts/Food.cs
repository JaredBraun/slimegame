﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public enum FoodTypes { Apple, Banana, Strawberry, Kiwi, Pear }

public class Food : Collectable {

    public bool isTaken;

    public FoodTypes type = FoodTypes.Apple;

    public override void OnEnable()
    {
        base.OnEnable();
        OnHitCollision.AddListener(ReactToAI);
    }

    public override void OnDisable()
    {
        OnHitCollision.RemoveListener(ReactToAI);
    }

    public void MoveToPlayer(Transform player)
    {
        Sequence mySequence = DOTween.Sequence();
        // Add a movement tween at the beginning
        mySequence.Append(transform.DOMove(transform.position, 0.3f));
        // Add a Scaling tween same time as the previous one
        mySequence.Join(transform.DOScale(0, 0.3f));
        mySequence.AppendCallback(()=> gameObject.SetActive(false));
    }

    void ReactToAI(Collider coll)
    {
        if (coll.GetComponent<AIController>() != null)
        {
            coll.GetComponent<AIController>().SetCurrentFood(this.gameObject);
        }
    }
}
