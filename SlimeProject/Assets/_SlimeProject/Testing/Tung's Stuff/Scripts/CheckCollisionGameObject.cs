﻿using NodeCanvas.Framework;
using ParadoxNotion;
using ParadoxNotion.Design;
using UnityEngine;

[Category("System Events")]
[EventReceiver("OnCollisionEnter", "OnCollisionExit")]
public class CheckCollisionGameObject : ConditionTask<Collider> {

    public CollisionTypes checkType = CollisionTypes.CollisionEnter;

    public BBParameter<GameObject> gameObjectToCheck;

    //[BlackboardOnly]
    //public BBParameter<GameObject> saveGameObjectAs;
    //[BlackboardOnly]
    //public BBParameter<Vector3> saveContactPoint;

    private bool stay;

    protected override string info
    {
        get { return "Check " + checkType.ToString() + " with " + gameObjectToCheck.name + " object"; }
    }

    protected override bool OnCheck()
    {
        if (checkType == CollisionTypes.CollisionStay)
            return stay;
        return false;
    }

    public void OnCollisionEnter(Collision info)
    {

        if (gameObjectToCheck.value == info.gameObject)
        {
            stay = true;
            if (checkType == CollisionTypes.CollisionEnter || checkType == CollisionTypes.CollisionStay)
            {
                //saveGameObjectAs.value = info.gameObject;
                //saveContactPoint.value = info.contacts[0].point;
                YieldReturn(true);
            }
        }
    }

    public void OnCollisionExit(Collision info)
    {

        if (gameObjectToCheck.value == info.gameObject)
        {
            stay = false;
            if (checkType == CollisionTypes.CollisionExit)
            {
                //saveGameObjectAs.value = info.gameObject;
                YieldReturn(true);
            }
        }
    }
}
