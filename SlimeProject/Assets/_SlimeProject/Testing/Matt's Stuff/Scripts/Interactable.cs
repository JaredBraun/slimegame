﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Interactable : MonoBehaviour
{
    public UnityEvent m_InteractEvent { get; private set; }

    public void InvokeActions()
    {
        m_InteractEvent.Invoke();
    }

    #region Action Methods
    public void AddAction(UnityAction action)
    {
        m_InteractEvent.AddListener(action);
    }

    public void RemoveAction(UnityAction action)
    {
        m_InteractEvent.RemoveListener(action);
    }

    public void RemoveAllActions()
    {
        m_InteractEvent.RemoveAllListeners();
    }
    #endregion

}