﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunVolume : MonoBehaviour
{
    public List<GameObject> gosInsideVolume;

    // Use this for initialization
    void Start () {
        gosInsideVolume = new List<GameObject>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerStay(Collider other)
    {

        if (other.gameObject.GetComponent<Collectable>() != null && !gosInsideVolume.Contains(other.gameObject))
        {
            gosInsideVolume.Add(other.gameObject);
        }

       
    }

    private void OnTriggerExit(Collider other)
    {
        gosInsideVolume.Clear();
    }
}
