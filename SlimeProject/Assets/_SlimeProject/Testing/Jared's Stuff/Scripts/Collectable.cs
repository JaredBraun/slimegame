﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[SerializeField]
public enum ItemType
{
    SLIME,
    ITEM,
    FOOD,
    NONE
}

[System.Serializable]
public class UnityColliderEvent : UnityEvent<Collider> { }

[RequireComponent(typeof(Rigidbody))]
public class Collectable : MonoBehaviour
{
    [Header("Settings")]
    public ItemType itemType = ItemType.ITEM;
    public string itemName;
    public int itemSize = 1;
    public float itemWorth = 1f;
    public Sprite inventoryIcon;
    public Color inInventoryColor = Color.white;
    public bool inInventory = false;

    [Header("Events")]
    public UnityColliderEvent OnHitCollision;
    public UnityEvent OnPulled;
    public UnityEvent OnShoot;

    [Header("Stats")]
    public bool isShot;
    public bool isPulled;

    [HideInInspector]
    public Rigidbody rb;

    public virtual void OnEnable() {
        if (OnHitCollision == null) OnHitCollision = new UnityColliderEvent();
        if (OnPulled == null) OnPulled = new UnityEvent();
    }
    public virtual void OnDisable() { }

    public virtual void Awake() { }
    public virtual void Start() {

        rb = GetComponent<Rigidbody>();
    }

    public void Update() {

        if(isShot == true && rb.velocity.magnitude < 0.5f) isShot = false;
    }

    public void PullTrigger(bool on)
    {
        if (on && !isPulled)
        {
            isPulled = true;
            OnPulled.Invoke();
            
        }
        if(!on && isPulled)
        {
            isPulled = false;
            OnPulled.Invoke();
        }
    }

    public Collectable(ItemType _itemTypeARG = ItemType.FOOD, 
        string _itemNameARG = "Collectable", int _itemSizeARG = 1, float _itemWorthARG = 1f, 
        Sprite _invenotryIconARG = null, bool _inInventoryARG = false)
    {
        this.itemType = _itemTypeARG;
        this.itemName = _itemNameARG;
        this.itemSize = _itemSizeARG;
        this.itemWorth = _itemWorthARG;
        this.inventoryIcon = _invenotryIconARG;
        this.inInventory = _inInventoryARG;
    }

    public void OnTriggerEnter(Collider coll)
    {
        if (isShot && OnHitCollision != null)
            OnHitCollision.Invoke(coll);
    }

}
