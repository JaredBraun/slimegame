﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[System.Serializable]
public class Slots
{
    [Header("The Slot's GameObject")]
    public GameObject theSlot;

    [Header("Graphics")]
    public Sprite emptySprite;
    private Sprite slotSprite;
    public Outline outline;
    public Image fillImg;
    public Image slotImage;
    public DOTweenAnimation keySelection;

    [Header("Max Size of Slot")]
    public int MAXSIZE = 100;

    [Header("Slot Data")]
    public bool selected;
    public Text slotAmount;
    public string slotItemName;
    public int slotItemCount;
    
    //items in the slot
    public List<Collectable> collectables = new List<Collectable>();

    public bool isFull()
    {
        int totalItemCount = 0;

        for (int i = 0; i < collectables.Count; i++)
        {
            totalItemCount += collectables[i].itemSize;
        }

        if (totalItemCount >= MAXSIZE)
        {
            fillImg.fillAmount = 1f;
            return true;

        }
        else
        {
            return false;
        }
    }

    public bool isEmpty()
    {
        if (collectables.Count == 0)
        {
            slotItemName = "";
            slotSprite = emptySprite;
            slotAmount.text = "";
            fillImg.fillAmount = 0f;
            return true;
        }
        else
        {
            return false;
        }
    }

    public void AddToSlot(Collectable collectable)
    {
        if (!isFull())
        {
            if (collectable != null && !collectables.Contains(collectable))
            {
                slotItemCount += collectable.itemSize;
                collectables.Add(collectable);
                slotItemName = collectable.itemName;
                collectable.inInventory = true;
                DetermineImage();
                DetermineSlotAmount();
                DetermineImageFill();
            }

        }

    }

    public void RemoveFromSlot(Collectable collectable)
    {
        if (collectable != null)
        {
            slotItemCount -= collectable.itemSize;
            collectable.inInventory = false;
            collectables.Remove(collectable);
            DetermineImage();
            DetermineSlotAmount();
            DetermineImageFill();

        }


    }

    public void RemoveFromSlotLocation(int index)
    {
        if (collectables[index] != null)
        {
            slotItemCount -= collectables[index].itemSize;
            collectables[index].inInventory = false;
            collectables.RemoveAt(index);
            DetermineImage();
            DetermineSlotAmount();
            DetermineImageFill();

        }


    }

    public void DetermineImage()
    {

        //slotImage = collectables[0].inventoryIcon;
        if (slotImage == null)
        {
            slotImage = theSlot.GetComponentsInChildren<Image>()[1];
        }

        if (!isEmpty())
        {
            slotImage.sprite = collectables[0].inventoryIcon;
        }
        else if (isEmpty())
        {
            slotImage.sprite = emptySprite;

        }


    }

    public void DetermineSlotAmount()
    {
        if (slotAmount == null)
        {
            slotAmount = theSlot.GetComponentInChildren<Text>();
        }


        slotAmount.text = collectables.Count.ToString();
        if (slotItemCount == 0)
        {
            slotAmount.text = "";
        }
    }

    public void ShowOutlines()
    {
        if (selected)
        {
            outline.effectColor = new Color32(255, 195, 0, 171);
        }
        else
        {
            outline.effectColor = Color.clear;
        }
    }

    public void DetermineImageFill()
    {
        int totalItemCount = 0;

        for (int i = 0; i < collectables.Count; i++)
        {
            totalItemCount += collectables[i].itemSize;
        }
        fillImg.fillAmount = ((float)totalItemCount / (float)MAXSIZE);

        if (isEmpty())
        {
            fillImg.color = Color.clear;
        }

        if (!isEmpty() && collectables[0] != null)
        {
            fillImg.color = collectables[0].inInventoryColor;
        }
    }
}

public class InventoryManager : MonoBehaviour
{
    #region Instance
    private static InventoryManager _instance;

    public static InventoryManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<InventoryManager>();
            }

            return _instance;
        }
    }
    #endregion



    public List<GameObject> inventory; //shooting and sucking gameObjects

    [SerializeField]
    public Slots[] slots;
    [HideInInspector]
    public int ActiveSlot;
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);

    }

    // Use this for initialization
    void Start()
    {
        inventory = new List<GameObject>();

        for (int i = 0; i < slots.Length; i++)
        {
            slots[i].isEmpty();
            slots[i].DetermineSlotAmount();
            slots[i].DetermineImage();
            
        }

    }

    // Update is called once per frame
    void Update()
    {

        for (int itemIndex = 0; itemIndex < inventory.Count; itemIndex++) //adding Collectables a list from Inventory
        {
            for (int slotIndex = 0; slotIndex < slots.Length; slotIndex++)
            {
                if ((slots[slotIndex].selected == true) && (!slots[slotIndex].isFull()))
                {
                    if (!inventory[itemIndex].GetComponent<Collectable>().inInventory)
                    {
                        slots[slotIndex].AddToSlot(inventory[itemIndex].GetComponent<Collectable>());

                    }

                }
            }



        }


        ActiveSlot = activeSlot();
        for (int i = 0; i < slots.Length; i++)
        {
            slots[i].ShowOutlines();
        }

    }


    /// <summary>
    /// Used for key selection 1-5 slots
    /// </summary>
    private void OnGUI()
    {
        #region Inventory KeyBoard Input
        Event input = Event.current;



        if (input.keyCode == KeyCode.Alpha1
            || input.keyCode == KeyCode.Alpha2
            || input.keyCode == KeyCode.Alpha3
            || input.keyCode == KeyCode.Alpha4
            || input.keyCode == KeyCode.Alpha5)
        {

            for (int i = 0; i < slots.Length; i++)
            {
                if (!slots[i].selected != true)
                    slots[i].selected = false;
            }
            slots[(int)input.keyCode - 49].selected = true;
            slots[(int)input.keyCode - 49].keySelection.DORestart(); //able to play more than once
        }
        #endregion
    }

    private int activeSlot()
    {
        int x = 0;
        for (x = 0; x < slots.Length; x++)
        {
            if (slots[x].selected)
            {
                return x;
            }


        }

        return x;

    }

    private int[] NonActiveSlots()
    {
        int[] nonActive = new int[slots.Length -1];
        for (int i = 0; i < slots.Length; i++)
        {
            if (!slots[i].selected)
            {
                nonActive[i] = i;
            }
        }

        return nonActive;
    }
}


