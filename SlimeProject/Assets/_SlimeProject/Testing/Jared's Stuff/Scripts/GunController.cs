﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class GunController : MonoBehaviour
{
    public GunVolume gunVolume;

    public float floatingPoint;
    public float pullForce = 1, pushForce = 1f;
    public Transform volumeOrigin, ShootPoint, SuckOrigin;

    private bool allowFire = true;
    public float fireRate = 1f;

    private DOTweenAnimation SuckAnimation;
    private DOTweenAnimation ShootAnimation;
    public DOTweenAnimation CameraShakeAnimation;
    public Sprite leftMouse, rightMouse, noMouse;
    private AudioSource SuckInSound, ShootSound;
    public Image MouseImage;

    private float distanceToOrigin;
    void Start()
    {
        SuckAnimation = GetComponents<DOTweenAnimation>()[0];
        ShootAnimation = GetComponents<DOTweenAnimation>()[1];
        //CameraShakeAnimation = Camera.main.GetComponent<DOTweenAnimation>();
        SuckInSound = GetComponents<AudioSource>()[0];
        ShootSound = GetComponents<AudioSource>()[1];


    }

    private void Update()
    {

        for (int i = 0; i < InventoryManager.Instance.inventory.Count; i++)
        {
            if (InventoryManager.Instance.inventory[i] != null)
            {
                InventoryManager.Instance.inventory[i].transform.position = new Vector3(ShootPoint.position.x, ShootPoint.position.y, ShootPoint.position.z);
                InventoryManager.Instance.inventory[i].transform.rotation = new Quaternion(ShootPoint.rotation.x, ShootPoint.rotation.y, ShootPoint.rotation.z, ShootPoint.rotation.w);
                InventoryManager.Instance.inventory[i].GetComponent<Rigidbody>().isKinematic = true;
            }

        }

    }

    public int pitchIncMax;
    public int pitchCount = 0;
    void FixedUpdate()
    {
        for (int i = 0; i < gunVolume.gosInsideVolume.Count; i++)
        {
            if (gunVolume.gosInsideVolume[i].activeInHierarchy)
            {
                if (pitchIncMax < gunVolume.gosInsideVolume.Count)
                {
                    pitchIncMax++;
                }

            }

            if (!gunVolume.gosInsideVolume[i].activeInHierarchy)
            {
                if (pitchIncMax > 0)
                {
                    pitchIncMax--;
                }
            }

        }



        
        if (Input.GetMouseButtonUp(0) ) //left mouse button releases
        {
            for (int i = 0; i < gunVolume.gosInsideVolume.Count; i++)
            {
                gunVolume.gosInsideVolume[i].GetComponent<Collectable>().PullTrigger(false);
            }

            SuckAnimation.DORewind(); //("Suck");
            MouseImage.sprite = noMouse;
        }

        if (Input.GetMouseButtonUp(1))
        {
            MouseImage.sprite = noMouse;

        }
        if (Input.GetMouseButton(0) && !Input.GetMouseButton(1)) //left mouse button down
        {
            MouseImage.sprite = leftMouse;
            for (int i = 0; i < gunVolume.gosInsideVolume.Count; i++)
            {

                gunVolume.gosInsideVolume[i].GetComponent<Collectable>().PullTrigger(true);
                // calculate direction from target to me
                Vector3 forceDirection = SuckOrigin.position - gunVolume.gosInsideVolume[i].transform.position;

                // apply force on target towards me

                gunVolume.gosInsideVolume[i].GetComponent<Rigidbody>().AddForce(forceDirection.normalized * pullForce * Time.fixedDeltaTime);
                distanceToOrigin = Vector3.Distance(volumeOrigin.position, gunVolume.gosInsideVolume[i].transform.position);
                gunVolume.gosInsideVolume[i].transform.forward = Vector3.Slerp(gunVolume.gosInsideVolume[i].transform.forward, -transform.forward, .08f);
                if (distanceToOrigin < .5f || distanceToOrigin < (Vector3.one * floatingPoint).magnitude)
                {
                    //gunVolume.gosInsideVolume[i].SetActive(false);
                    if (!InventoryManager.Instance.inventory.Contains(gunVolume.gosInsideVolume[i]))
                    {


                        for (int x = 0; x < InventoryManager.Instance.slots.Length; x++)
                        {
                            if (!InventoryManager.Instance.slots[InventoryManager.Instance.ActiveSlot].isFull() &&
                                !InventoryManager.Instance.inventory.Contains(gunVolume.gosInsideVolume[i]) &&
                                (InventoryManager.Instance.slots[InventoryManager.Instance.ActiveSlot].slotItemName == gunVolume.gosInsideVolume[i].GetComponent<Collectable>().itemName || (InventoryManager.Instance.slots[InventoryManager.Instance.ActiveSlot].slotItemName) == ""))
                            {
                                SuckInSound.Play();
                                if (pitchCount < pitchIncMax)
                                {
                                    SuckInSound.pitch += .065f;
                                    pitchCount++;
                                }
                                else if (pitchCount >= pitchIncMax && pitchCount != 0)
                                {

                                    SuckInSound.pitch = 1f;


                                    pitchCount = 0;
                                }
                                InventoryManager.Instance.inventory.Add(gunVolume.gosInsideVolume[i]);
                                gunVolume.gosInsideVolume[i].GetComponent<Collectable>().PullTrigger(false);
                                gunVolume.gosInsideVolume[i].SetActive(false);
                            }
                            else
                            {
                                //slot is full or cant match slot name
                            }
                        }




                    }


                }
            }

            SuckAnimation.DOPlayForwardById("Suck");

        }
        if (Input.GetMouseButton(1) && !Input.GetMouseButton(0)) //right mouse button down
        {
            MouseImage.sprite = rightMouse;
            if (InventoryManager.Instance.inventory.Count > 0 && (allowFire))
            {
                StartCoroutine(ShootOut(InventoryManager.Instance.inventory));
            }
            



        }
    }

    IEnumerator ShootOut(List<GameObject> gos)
    {
        int index = InventoryManager.Instance.ActiveSlot;
        int inventoryItem = gos.Count - 1;

        allowFire = false;
        //SuckAnimation.enabled = false;


        if (gos[inventoryItem] != null && !InventoryManager.Instance.slots[index].isEmpty())
        {
            gos[inventoryItem].SetActive(true);
            InventoryManager.Instance.inventory[inventoryItem].GetComponent<Rigidbody>().isKinematic = false;
            ShootAnimation.DOPlayById("Shoot");
            ShootSound.Play();
            CameraShakeAnimation.DOPlay();
            gos[inventoryItem].GetComponent<Rigidbody>().AddForce(ShootPoint.forward * pushForce);
            gos[inventoryItem].GetComponent<Collectable>().isShot = true;
            gos.RemoveAt(inventoryItem);
            ShootAnimation.DORestartById("Shoot");
            CameraShakeAnimation.DORestart();


            //removes item from slot list latest to earliest
            for (int i = InventoryManager.Instance.slots[index].collectables.Count - 1; i > -1; i--)
            {
                
                InventoryManager.Instance.slots[index].RemoveFromSlotLocation(i);
            }

        }
        else
        {
            Debug.Log("Empty Inventory");
        }
        yield return new WaitForSeconds(fireRate);
        allowFire = true;





    }


}//end class
